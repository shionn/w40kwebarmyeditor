/**
 * 
 */

'use strict';

var INFANTERY = 0;
var MOTO = 0;
var TANK = 1;
var WALKER = 2;
var AERO = 1;

var MODEL = [ {
	root : 'UNIT',
	composition : 'UNIT-LINE'
}, {
	root : 'TANK',
	composition : 'TANK-LINE'
}, {
	root : 'WALKER',
	composition : 'WALKER-LINE'
} ]

var ARMYS = [];

var arme = function(name, qty, pt) {
	return {
		name : name,
		qty : qty,
		pt : pt
	};
};

var character = function(qty, name, cc, ct, f, e, pv, i, a, cd, svg, pt) {
	return {
		name : name,
		qty : qty,
		pt : pt,
		cc : cc,
		ct : ct,
		f : f,
		e : e,
		pv : pv,
		i : i,
		a : a,
		cd : cd,
		svg : svg
	};
};

var walker = function(qty, name, cc, ct, f, av, fl, arr, i, a, pc, pt) {
	return {
		name : name,
		qty : qty,
		pt : pt,
		cc : cc,
		ct : ct,
		f : f,
		av : av,
		fl : fl,
		arr : arr,
		pc : pc,
		i : i,
		a : a
	};
};

var tank = function(qty, name, ct, av, fl, arr, pc, pt) {
	return {
		name : name,
		qty : qty,
		pt : pt,
		ct : ct,
		av : av,
		fl : fl,
		arr : arr,
		pc : pc
	};
};
