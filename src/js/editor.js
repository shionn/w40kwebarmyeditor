'use strict';

var UnitEditor = function(unit, armyEditor) {
	this.updatePts = function() {
		unit.pts = 0;
		$.each(unit.composition, function() {
			unit.pts += this.qty * this.pt;
		});
		$.each(unit.equip, function() {
			unit.pts += this.qty * this.pt;
		});
		root.find("[var=pts]").text(unit.pts);
		armyEditor.updatePts();
	};

	this.buildCompositionHtml = function(composition) {
		var line = new Template(MODEL[unit.type].composition, false).getDom();
		$(root).find("tbody").append(line);
		new InlineVue(line, composition);
		new InlineEditor(line, composition, $.proxy(this.updatePts, this));
		line.find("[role=delete]").on("click", $.proxy(function() {
			var i = unit.composition.indexOf(composition);
			if (i >= 0 && unit.composition.length >= 1) {
				unit.composition.splice(i, 1);
				line.remove();
				this.updatePts();
			}
		}, this));
		line.find("[role=create]").on("click", $.proxy(function() {
			var one = $.extend(true, {}, composition);
			unit.composition.push(one);
			this.buildCompositionHtml(one);
			this.updatePts();
		}, this));
	};

	this.buildEquipHtml = function(equip) {
		var line = new Template("EQUIP", false).getDom();
		$(root).find("[role=list-equip]").append(line);
		new InlineVue(line, equip);
		new InlineEditor(line, equip, $.proxy(this.updatePts, this));
		line.find("[role=delete]").on("click", $.proxy(function() {
			var i = unit.equip.indexOf(equip);
			if (i >= 0) {
				unit.equip.splice(i, 1);
				line.remove();
				this.updatePts();
			}
		}, this));
	};

	this.createEquip = function() {
		var equip = {
			name : 'equipement / option',
			qty : 1,
			pt : 5,
		};
		unit.equip.push(equip);
		this.buildEquipHtml(equip);
		this.updatePts();
	};

	var root = new Template(MODEL[unit.type].root).appendTo("#UNITS");
	root.on("ADD-EQUIP", "click", {}, $.proxy(this.createEquip, this));
	root.on("delete", "click", function() {
		armyEditor.remove(unit);
		root.remove();
	});
	root.on("up", "click", function() {
		armyEditor.up(unit);
	});
	root.on("down", "click", function() {
		armyEditor.down(unit);
	});
	root = root.getDom();
	new InlineVue(root, unit);
	new InlineEditor(root, unit, $.proxy(this.updatePts, this));

	$.each(unit.composition, $.proxy(function(i, composition) {
		this.buildCompositionHtml(composition);
	}, this));

	$.each(unit.equip, $.proxy(function(i, equip) {
		this.buildEquipHtml(equip);
	}, this));

};

var ArmyEditor = function() {
	var army = {
		name : "army name",
		pts : 0,
		units : []
	};
	var html = $("#ARMY");

	this.updatePts = function() {
		army.pts = 0;
		$.each(army.units, function() {
			army.pts += this.pts;
		});
		html.find("[var=pts]").text(army.pts);
	};

	this.refreshView = function() {
		$("#UNITS").empty();
		new InlineVue(html, army);
		new InlineEditor(html, army, $.proxy(this.updatePts, this));
		$.each(army.units, $.proxy(function(i, unit) {
			new UnitEditor(unit, this);
		}, this));
		this.updatePts();
	};

	this.create = function(unit) {
		army.units.push(unit);
		new UnitEditor(unit, this);
		this.updatePts();
	};

	this.remove = function(unit) {
		var i = army.units.indexOf(unit);
		if (i >= 0) {
			army.units.splice(i, 1);
			this.updatePts();
		}
	};

	this.up = function(unit) {
		var i = army.units.indexOf(unit);
		if (i >= 1) {
			army.units.splice(i, 1);
			army.units.splice(i - 1, 0, unit);
			this.refreshView();
		}
	};

	this.down = function(unit) {
		var i = army.units.indexOf(unit);
		if (i >= 0 && i < army.units.length - 1) {
			army.units.splice(i, 1);
			army.units.splice(i + 1, 0, unit);
			this.refreshView();
		}
	};

	this.save = function() {
		var url = 'data:Application/octet-stream;charset=utf8,' + JSON.stringify(army);
		window.open(url, '_blank');
		window.focus();
	};

	this.load = function() {
		var finput = document.getElementById("load");
		if (finput.files.length > 0) {
			var reader = new FileReader();
			reader.onload = $.proxy(function(e) {
				army = JSON.parse(e.target.result);
				this.refreshView();
			}, this);
			reader.readAsText(finput.files[0]);
		}
	};

	this.refreshView();

};

$(function() {
	var editor = new ArmyEditor();

	$.each(ARMYS, function(i, army) {
		var root = new Template("army-menu", false).appendTo("[role=army-menu-list]").text("button",
				army.name).find("menu");
		var lastGr = army.units[0].group;
		$.each(army.units, function() {
			if (this.group !== lastGr) {
				lastGr =  this.group;
				$('<li class="divider"></li>').appendTo(root);
			}
			new Template("army-menu-item", false).on("add", "click", {
				unit : this
			}, function(e) {
				editor.create($.extend(true,{}, e.data.unit));
			}).text("add", this.name).appendTo(root);
		});

	});

	$("#save").on("click", function() {
		editor.save();
	});

	$("#load").on("change", function() {
		editor.load();
	});

});