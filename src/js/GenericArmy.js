/**
 * Liste des unitées généric
 */

'use strict';

$(function() {
	ARMYS.push({
		name : 'Générique',
		units : [ {
			type : INFANTERY,
			name : 'Infanterie',
			group : 'Troupe',
			pts : 15,
			description : "Description / regle / ...",
			composition : [ character(1, 'Nom', 4, 4, 4, 4, 1, 4, 1, 4, "4+", 10) ],
			equip : [ arme('equipement / option', 1, 5) ]
		}, {
			type : TANK,
			name : 'Véhicule',
			group : 'Transport',
			pts : 50,
			description : "Description / regle / ...",
			composition : [ tank(1, 'Nom', 4, 11, 11, 11, 3, 50) ],
			equip : [ arme('equipement / option', 1, 5) ]
		}, {
			type : WALKER,
			name : 'Marcheur',
			group : 'Elite',
			pts : 100,
			description : "Description / regle / ...",
			composition : [ walker(1, 'Nom', 4, 4, 4, 11, 11, 11, 4, 4, 3, 50) ],
			equip : [ arme('equipement / option', 1, 5) ]
		} ]
	});
})
