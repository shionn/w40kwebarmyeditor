/**
 * Liste des unitée disponible chez les darkangels
 */

'use strict';

$(function() {
	var ARME_DE_TIR = [ arme('Bolter', 0, 0),
			arme("Bolter d'assaut", 0, 5),
			arme("Combi-fuseur", 0, 10),
			arme("Combi-plasma", 0, 10),
			arme("Combi-lance-flamme", 0, 10),
			arme("Pistolet à plasma", 0, 15) ];

	var ARME_DE_MELEE = [ arme('Epée tronçonneuse', 0, 0),
			arme("Arme énergétique", 0, 15),
			arme("Griffe éclair", 0, 15),
			arme("Gantelet énergétique", 0, 25),
			arme("Marteau tonnerre", 0, 30) ];

	var SPECIALS = [ arme("Auspex", 0, 5),
			arme("Bombes à fusion", 0, 5),
			arme("Bouclier de combat", 0, 5),
			arme("Infravisière", 0, 5),
			arme("Chevalet de torture", 0, 10),
			arme("Digilasers", 0, 10),
			arme("Champ de conversion", 0, 15),
			arme("Réacteurs dorsaux", 0, 15),
			arme("Moto space marine", 0, 20),
			arme("Champs téléporteur", 0, 25),
			arme("Générateur de champ énergétique", 0, 30) ]

	var RELIQUE_CHAPITRALE = [ arme("Punisseur", 0, 20),
			arme("Rugissement du Lion", 0, 20),
			arme("Masse de Rédenption", 0, 30),
			arme("Tueuse de Monstre de Caliban", 0, 45),
			arme("Suaire des Héros", 0, 50) ];

	var ETENDARDS = [ arme("Etendard de Châtiment", 0, 45),
			arme("Etendard de Dévastation", 0, 65),
			arme("Etendard de Courage", 0, 85) ];

	var ARME_TERMINATOR = [ arme("Combi-fuseur", 0, 6),
			arme("Combi-plasma", 0, 6),
			arme("Combi-lance-flamme", 0, 6) ]

	var DARK_ANGEL = {
		name : 'Dark Angel',
		units : [ {
			type : INFANTERY,
			name : 'Azrael',
			group : 'QG',
			pts : 215,
			description : "Trait de seigneur de guerre: Si Azrael est dans une armée, il doit être seigneur de guerre, et il peut choisir un des traits de seigneur de guerre Dark Angels (sans lancer le dé). \n"
					+ "Grand Maître suprême des Dark Angels: Dans un détachement principal qui inclut Azrael, les escadrons d'attaque de la RavenWing et les escoudaes Terminator de la Deathwing sont des choix de Troupes au lieu de leur catégorie habituelle sur le schéma de structure. \n"
					+ "Régles Spéciales: Cercle Intérieur, Personnage indépendant, Rites de bataille",
			composition : [ character(1, 'Azrael', 6, 5, 4, 4, 4, 5, 4, 10, "2+/4+", 215) ],
			equip : [ arme('Grenades antichars', 1, 0),
					arme('Grenades frag', 1, 0),
					arme('Pistolet bolter', 1, 0),
					arme('Courroux du Lion', 1, 0),
					arme('Epée des secrets', 1, 0),
					arme('Heaume du Lion', 1, 0),
					arme('Protecteur', 1, 0) ]
		},
				{
					type : INFANTERY,
					name : 'Belial',
					group : 'QG',
					pts : 190,
					description : "Trait de seigneur de guerre: La Traque \n"
							+ "Grand Maître de la Deathwing: Dans un détachement principal qui inclut Belial, les escoudes de terminators Deathwing sont des choix de Troupes au lieu d'Elite.\n"
							+ "Régles Spéciales: Assaut Deathwing, Cercle Intérieur, Exactitude tactique, Frappe vengeresse, La marque du châtiment, Personnage indépendant",
					composition : [ character(1, 'Belial', 6, 5, 4, 4, 3, 5, 3, 10, "2+/?+", 190) ],
					equip : [ arme('Armure Terminator', 1, 0),
							arme('Balise de téléportation', 1, 0),
							arme("Bolter d'assaut", 1, 0),
							arme('Halo de fer', 1, 0),
							arme('Epée du Silence', 1, 0) ]
				},
				{
					type : INFANTERY,
					name : 'Maître de Compagnie',
					group : 'QG',
					pts : 90,
					description : "Régles Spéciales:  Cercle Intérieur",
					composition : [ character(1, 'Mare de Compagnie', 6, 5, 4, 4, 3, 5, 3, 10, "3+/?+", 90) ],
					equip : [ arme('Armure énergétique', 1, 0),
							arme('Epée tronçonneuse', 1, 0),
							arme("Grenade antichar", 1, 0),
							arme("Grenade frag", 1, 0),
							arme('Halo de fer', 1, 0),
							arme('Pistolet bolter', 1, 0),
							arme('Bouclier tempête', 0, 15),
							arme('Relique de préjudice des Impardonnés', 0, 15),
							arme("Armure d'artificié", 0, 20) ].concat(ARME_DE_TIR).concat(ARME_DE_MELEE).concat(
							RELIQUE_CHAPITRALE).concat(SPECIALS)
				},
				{
					type : INFANTERY,
					name : 'Archiviste',
					group : 'QG',
					pts : 65,
					description : "Régles Spéciales:  Cercle Intérieur, Psyker (1, Divination, Pyromancie, Télépathie, Télékinésie)",
					composition : [ character(1, 'Archiviste', 5, 4, 4, 4, 2, 4, 3, 10, "3+", 65) ],
					equip : [ arme('Armure énergétique', 1, 0),
							arme('Arme de force', 1, 0),
							arme("Grenade antichar", 1, 0),
							arme("Grenade frag", 1, 0),
							arme('Coiffe psychique', 1, 0),
							arme('Pistolet bolter', 1, 0),
							arme('Psyker (2)', 0, 35) ]
				},
				{
					type : INFANTERY,
					name : 'Maître de Compagnie (Terminator)',
					group : 'QG',
					pts : 130,
					description : "Régles Spéciales:  Cercle Intérieur",
					composition : [ character(1, 'Mare de Compagnie', 6, 5, 4, 4, 3, 5, 3, 10, "2+/?+", 130) ],
					equip : [ arme('Armure Terminator', 1, 0),
							arme("Bolter d'assaut", 1, 0),
							arme('Epée énergétique', 1, 0),
							arme('Halo de fer', 1, 0),
							arme('Relique de préjudice des Impardonnés', 0, 20) ].concat(SPECIALS).concat(
							RELIQUE_CHAPITRALE).concat(ARME_TERMINATOR)
				},
				{
					type : INFANTERY,
					name : "Escouade de commandement de la Deathwing",
					group : "QG",
					pts : 220,
					description : "Régles Spéciales: Assaut Deathwing, Cercle Intérieur, Frappe vengeresse, Tir divisé",
					composition : [ character(5, 'Terminator Deathwing', 4, 4, 4, 4, 1, 4, 2, 9, "2+/6+", 44),
							character(0, 'Apothicaire Deathwing', 4, 4, 4, 4, 1, 4, 2, 9, "2+/6+", 74),
							character(0, 'Champion Deathwing', 5, 4, 4, 4, 1, 4, 2, 9, "2+/6+", 49) ],
					equip : [ arme('Armure Terminator', 5, 0),
							arme("Gantelet énergétique", 5, 0),
							arme("Bolter d'assaut", 5, 0),
							// 1 seul (avec ETENDARDS)
							arme("Bannière de compagnie de la Deathwing", 0, 45),
							arme("Etendard vénéré", 0, 20),
							// pour le champion
							arme('Hallebarde de Caliban', 0, 0),
							// pour l'apoticaire
							arme('Narthecium', 0, 0),
							// un seul
							arme('Lance-flammes lourd', 0, 10),
							arme('Canon à plasma', 0, 15),
							arme("Canon d'assaut", 0, 20),
							arme('Lance-missiles Cyclone', 0, 25),
							// n'importe remplace les deux
							arme("Pair de griffes éclair", 0, 0),
							arme("Marteau tonnerre & bouclier tempête", 0, 5),
							// n'importe remplace le gantelet
							arme("Point tronçonneur", 0, 5) ].concat(ETENDARDS)
				},
				{
					type : INFANTERY,
					name : 'Escouade Tactique',
					group : 'Troupes',
					pts : 70,
					description : "Régles Spéciales: Âpre résolution, Et ils ne connaîtrons pas la peur, Escouades de combat",
					composition : [ character(4, 'Space marine', 4, 4, 4, 4, 1, 4, 1, 8, "3+", 14),
							character(1, 'Sergent space marine', 4, 4, 4, 4, 1, 4, 1, 8, "3+", 14),
							character(0, 'Sergent vétéran', 4, 4, 4, 4, 1, 4, 2, 9, "3+", 24) ],
					equip : [ arme('Armure énergétique', 5, 0),
							arme('Bolter', 4, 0),
							arme('Pistolet bolter', 1, 0),
							arme('Grenade frag', 5, 0),
							arme('Grenade antichar', 5, 0),
							arme('Lance-flammes', 0, 5),
							arme('Fuseur', 0, 10),
							arme('Fusil à plasma', 0, 15),
							arme('Bolter lourd', 0, 10),
							arme('Multi-fuseur', 0, 10),
							arme('Canon à plasma', 0, 15),
							arme('Lance-missiles (frag/antichars)', 0, 15),
							arme('Missiles antiaériens', 0, 10),
							arme('Canon laser', 0, 10),
							arme('Bombes à fusion', 0, 5) ].concat(ARME_DE_TIR).concat(ARME_DE_MELEE)
				},
				{
					type : INFANTERY,
					name : "Escouade de vétérans de compagnie",
					group : "Elite",
					pts : 90,
					description : "Régles Spéciales: Âpre résolution, Et ils ne connaîtrons pas la peur, Escouades de combat",
					composition : [ character(4, 'Vétéran', 4, 4, 4, 4, 1, 4, 2, 9, "3+", 18),
							character(1, 'Sergent vétéran', 4, 4, 4, 4, 1, 4, 2, 9, "3+", 18) ],
					equip : [ arme('Armure énergétique', 5, 0),
							arme('Bolter', 4, 0),
							arme('Pistolet bolter', 1, 0),
							arme('Grenade frag', 5, 0),
							arme('Grenade antichar', 5, 0),
							arme('Epée tronçonneuse', 0, 0),
							// jusqu'a3
							arme("Bolter d'assaut", 0, 5),
							arme("Combi-fuseur", 0, 10),
							arme("Combi-plasma", 0, 10),
							arme("Combi-lance-flamme", 0, 10),
							arme("Pistolet à plasma", 0, 15),
							arme("Arme énergétique", 0, 15),
							arme("Griffes éclair", 0, 15),
							arme("Gantelet énergétique", 0, 25),
							arme("Pair de griffes éclair", 0, 30),
							// chacun
							arme('Bouclier de combat', 0, 5),
							arme('Bombes à fusion', 0, 5),
							arme('Bouclier Tempête', 0, 10),
							arme('Bolter', 0, 0),
							// 1 seul
							arme('Lance-flammes', 0, 5),
							arme('Fuseur', 0, 10),
							arme('Fusil à plasma', 0, 15),
							// 1 seul
							arme('Bolter lourd', 0, 10),
							arme('Multi-fuseur', 0, 10),
							arme('Canon à plasma', 0, 15),
							arme('Lance-missiles (frag/antichars)', 0, 15),
							arme('Missiles antiaériens', 0, 10),
							arme('Canon laser', 0, 10) ]
				},
				{
					type : INFANTERY,
					name : "Escouade de terminator de la Deathwing",
					group : "Elite",
					pts : 220,
					description : "Régles Spéciales: Assaut Deathwing, Cercle Intérieur, Frappe vengeresse, Tir divisé",
					composition : [ character(4, 'Terminator Deathwing', 4, 4, 4, 4, 1, 4, 2, 9, "2+/6+", 44),
							character(1, 'Sergent Terminator Deathwing', 4, 4, 4, 4, 1, 4, 2, 9, "2+/6+", 44) ],
					equip : [ arme('Armure Terminator', 5, 0),
							arme("Gantelet énergétique", 4, 0),
							arme("Epée énergétique", 1, 0),
							arme("Bolter d'assaut", 5, 0),
							// n'importe remplace les deux
							arme("Pair de griffes éclair", 0, 0),
							arme("Marteau tonnerre & bouclier tempête", 0, 5),
							// n'importe remplace le gantelet
							arme("Point tronçonneur", 0, 5),
							// un seul
							arme('Lance-flammes lourd', 0, 10),
							arme('Canon à plasma', 0, 15),
							arme("Canon d'assaut", 0, 20),
							arme('Lance-missiles Cyclone', 0, 25) ]
				},
				{
					type : INFANTERY,
					name : "Chevalier de la Deathwing",
					group : "Elite",
					pts : 235,
					description : "Régles Spéciales: Assaut Deathwing, Cercle Intérieur, Foreresse de boucliers, Marteau de fureur, Vous ne pouvez vous cacher",
					composition : [ character(4, 'Chevalier de la Deathwing', 5, 4, 4, 4, 1, 4, 2, 9,
							"2+/6+", 46),
							character(1, 'Maître Chevalier', 5, 4, 4, 4, 1, 4, 3, 9, "2+/6+", 48) ],
					equip : [ arme('Armure Terminator', 5, 0),
							arme("Masse d'absolution", 4, 0),
							arme("Fléau des Impardonnés", 1, 0),
							arme("Bouclier Tempête", 5, 0),
							arme('Relique de préjudice des Impardonnés', 0, 10)]
				},
				{
					type : MOTO,
					name : "Escadron d'attaque de la Ravenwing",
					group : 'Attaque Rapide',
					pts : 80,
					description : "Régles Spéciales: Âpre résolution, Désangagement, Et ils ne connaîtrons pas la peur, Escouades de combat Ravenwing, Scouts",
					composition : [ character(2, 'Motard de la Ravenwing', 4, 4, 4, 5, 1, 4, 1, 8, "3+", 27),
							character(1, 'Sergent de la Ravenwing', 4, 4, 4, 5, 1, 4, 1, 8, "3+", 26),
							character(0, 'Sgt. vét de la Ravenwing', 4, 4, 4, 5, 1, 4, 2, 9, "3+", 36),
							character(0, "Moto d'assaut de la Ravenwing", 4, 4, 4, 5, 2, 4, 2, 8, "3+", 45) ],
					equip : [ arme('Armure énergétique', 3, 0),
							arme('Balise de téléportation', 3, 0),
							arme('Grenade frag', 3, 0),
							arme('Grenade antichar', 3, 0),
							arme('Pistolet bolter', 3, 0),
							arme('Bolter jumelés', 3, 0),
							arme('Lance-flammes', 0, 5),
							arme('Fuseur', 0, 10),
							arme('Fusil à plasma', 0, 15),
							arme("Bolter lourd (M. assaut)", 0, 0),
							arme("Multi-fuseur (M. assaut)", 0, 10) ].concat(ARME_DE_TIR).concat(ARME_DE_MELEE)
				},
				{
					type : MOTO,
					name : "Chevaliers noir de la Ravenwing",
					group : 'Attaque Rapide',
					pts : 126,
					description : "Régles Spéciales: Âpre résolution, Désangagement, Et ils ne connaîtrons pas la peur, Scouts, Pilote émérite",
					composition : [ character(2, 'Chevalier Noir', 4, 4, 4, 5, 1, 4, 2, 9, "3+", 42),
							character(1, 'Maître de la Traque', 4, 4, 4, 5, 1, 4, 2, 9, "3+", 42) ],
					equip : [ arme('Armure énergétique', 3, 0),
							arme('Balise de téléportation', 3, 0),
							arme('Grenade frag', 3, 0),
							arme('Grenade antichar', 3, 0),
							arme('Pistolet bolter', 3, 0),
							arme('Serre à plasma', 3, 0),
							arme('Marteau Corvus', 1, 0),
							arme('Lance grenade de la Ravenwing', 0, 0),
							arme('Arme énergétique (M Traque)', 0, 12),
							arme('Bombe (M Traque)', 0, 5) ]
				},
				{
					type : AERO,
					name : "Dark Talon de la Ravenwing",
					group : 'Attaque Rapide',
					pts : 160,
					description : "Régles Spéciales: Frappe stationnaire",
					composition : [ tank(1, 'Dark Talon de la Ravenwing', 4, 11, 11, 11, 3, 160) ],
					equip : [ arme('Bombe à stase', 1, 0),
							arme('Canon Rift', 1, 0),
							arme('Bolters Ouragan', 2, 0) ]
				} ]
	};

	ARMYS.push(DARK_ANGEL);

});
