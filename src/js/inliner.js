'use strict';

var InlineEditor = function(html, object, callback) {

	this.startEdit = function(event) {
		var span = $(event.target);
		if ("textarea" == span.attr("model")) {
			var input = $('<textarea class="form-control"></textarea>');
		} else {
			var l = span.outerWidth();
			l = Math.max(l, 100);
			var input = $('<input class="form-control form-control-inline" style="width:' + l + 'px"/>');
		}
		input.attr("type", span.attr("model"));
		input.insertAfter(span).focus().val(span.text()).select();
		input.on('blur', $.proxy(function(event) {
			span.removeClass('hidden');
			span.text(input.val());
			object[span.attr('var')] = input.val();
			input.remove();
			callback.call();
		}, this));
		span.addClass('hidden');
	};

	$.each(html.find(".editable"), $.proxy(function(i, span) {
		$(span).on("click", $.proxy(this.startEdit, this));
	}, this));
};

var InlineVue = function(html, object) {
	$.each(html.find("[var]"), function() {
		$(this).text(object[$(this).attr("var")]);
	});

	$.each($(".auto-hide"), function(i, btn) {
		$(btn).parent().mouseover(function() {
			$(btn).removeClass('hidden');
		});
		$(btn).parent().mouseout(function() {
			$(btn).addClass('hidden');
		});
		$(btn).addClass('hidden');
	});

};
