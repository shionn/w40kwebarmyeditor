'use strict';

var Template = function(name, cloneEvent) {
	this.template = $("#TEMPLATE").find("[role=" + name + "]").clone(cloneEvent);

	this.before = function(selector) {
		$(selector).before(this.template);
		return this;
	};

	this.after = function(selector) {
		$(selector).after(this.template);
		return this;
	};

	this.appendTo = function(selector) {
		$(selector).append(this.template);
		return this;
	};

	this.on = function(name, event, data, callback) {
		this.find(name).on(event, data, callback);
		return this;
	};

	this.text = function(name, value) {
		this.find(name).text(value);
		return this;
	};

	this.html = function(name, value) {
		this.find(name).html(value);
		return this;
	};

	this.val = function(name, value) {
		this.find(name).val(value);
		return this;
	};

	this.attr = function(name, attr, value) {
		this.find(name).attr(attr, value);
		return this;
	};

	this.remove = function(name) {
		this.find(name).remove();
	};

	this.exist = function(name) {
		return this.find(name).length > 0;
	};

	this.find = function(name) {
		return this.template.find("[role=" + name + "]");
	};

	this.getDom = function() {
		return this.template;
	};
};